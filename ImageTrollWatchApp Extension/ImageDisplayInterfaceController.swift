//
//  ImageDisplayInterfaceController.swift
//  ImageTrollWatchApp Extension
//
//  Created by pwnstarSmithy on 21/09/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import WatchKit
import Foundation

//Declare variable to cache images.
var postImageCache = NSCache<NSString, UIImage>()

//extension to download images from internet
extension WKInterfaceImage {
    
    public func setImageWithUrl(url:String, scale: CGFloat = 1.0) -> WKInterfaceImage? {
        
        URLSession.shared.dataTask(with: NSURL(string: url)! as URL) { data, response, error in
            if (data != nil && error == nil) {
                let image = UIImage(data: data!, scale: scale)
                
                DispatchQueue.main.async() {
                    self.setImage(image)
                }
            }
            }.resume()
        
        return self
    }
}
class ImageDisplayInterfaceController: WKInterfaceController {

    var finalLink : String?
    
    @IBOutlet var displayImage: WKInterfaceImage!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        showImage()
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    func showImage() {
        
        if imageArray.isEmpty {
            
            finalLink = imageLink
            
            self.displayImage.setImageWithUrl(url: finalLink!, scale: 1.0)
            
        }else {
            
         finalLink = imageArray.first?.link
            
            self.displayImage.setImageWithUrl(url: finalLink!, scale: 1.0)
            
        }
        
        
    }
    
}
