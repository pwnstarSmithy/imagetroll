//
//  InterfaceController.swift
//  ImageTrollWatchApp Extension
//
//  Created by pwnstarSmithy on 21/09/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import WatchKit
import Foundation

var defaultsId : Int?

class InterfaceController: WKInterfaceController {
    
    var defaultsArray = [FirstData]()
    

    
    @IBOutlet var tableView: WKInterfaceTable!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        print("Watch application loaded!")
        
        loadDefaults()
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    func loadDefaults() {
        
        
        
        let url = NSURL(string: "https://api.imgur.com/3/topics/defaults")!
        
        let tokenString = "Bearer " + accessToken
        
        var request = URLRequest(url: url as URL)
        
        request.setValue(tokenString, forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        //launch the session
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let data = data else {return}
            
            
            if error == nil {
                
                do {
                    
                    let pulledData = try JSONDecoder().decode(DefaultsWatch.self, from: data)
                    
                    self.defaultsArray = pulledData.data!
                    
                    self.tableView.setNumberOfRows(self.defaultsArray.count, withRowType: "RowController")
                    
                    for (index, rowModel) in self.defaultsArray.enumerated(){
                        
                        if let rowController = self.tableView.rowController(at: index) as? RowController{
                            rowController.rowLabel.setText(rowModel.name)
                        }
                        
                    }

                }catch{

                    print(error)
                    
                }
                
                
            }else{
                print(error!)
            }
            
            }.resume()
    
    }
    
    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
    
        
        
        pushController(withName: "ImagesListInterfaceController", context: nil)
   
        defaultsId = defaultsArray[rowIndex].id

    }
    
  
    
}
