//
//  ImagesListInterfaceController.swift
//  ImageTrollWatchApp Extension
//
//  Created by pwnstarSmithy on 21/09/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import Foundation

import WatchKit

var imageLink : String?
var imageArray = [Images]()

class ImagesListInterfaceController: WKInterfaceController {
   
    var imagesArray = [ImageArray]()
    
    @IBOutlet var imagesTableVIew: WKInterfaceTable!
    
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)    
        loadMoreImages()
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    func loadMoreImages() {
        
        let viral = "viral"
        let topicId = defaultsId
        let pageNumber = 1
        
        let url = NSURL(string: "https://api.imgur.com/3/topics/\(topicId!)/\(viral)/\(pageNumber)")!
        
        let tokenString = "Bearer " + accessToken
        
        var request = URLRequest(url: url as URL)
        
        request.setValue(tokenString, forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        //launch the session
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let data = data else {return}
            
            
            if error == nil {
                
                do {
                    
                    let pulledData = try JSONDecoder().decode(TopicsWatch.self, from: data)
                    
                    self.imagesArray = pulledData.data!
                    
                    self.imagesTableVIew.setNumberOfRows(self.imagesArray.count, withRowType: "ImagesRowController")
                    
                    for (index, rowModel) in self.imagesArray.enumerated(){
                        
                        if let rowController = self.imagesTableVIew.rowController(at: index) as? ImagesRowController{
                            rowController.imagesListLabel.setText(rowModel.title)
                        }
                        
                    }
                    
                }catch{
                    
                    print(error)
                    
                }
                
            }else{
                print(error!)
                
            }
            
            }.resume()
        
        
        
    }
    
    
    
    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
    
           pushController(withName: "ImageDisplayControler", context: nil)
        
        if imagesArray[rowIndex].images != nil {
      
            imageArray = imagesArray[rowIndex].images!
            
        }else{
            
            imageLink = imagesArray[rowIndex].link
            
        }
    }
    
    
    
    
}
