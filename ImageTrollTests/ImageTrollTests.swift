//
//  ImageTrollTests.swift
//  ImageTrollTests
//
//  Created by pwnstarSmithy on 17/09/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import XCTest

@testable import ImageTroll

class ImageTrollTests: XCTestCase {
    
    var testImage : UIImageView?

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    
    func testToken() {
        
         let expected = expectation(description: "The token is very much valid!")

        let url = NSURL(string: "https://api.imgur.com/3/topics/defaults")!
        
        let tokenString = "Bearer " + accessToken
        
        var request = URLRequest(url: url as URL)
        
        request.setValue(tokenString, forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        //launch the session
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let data = data else {return}
            
            
            if error == nil {
                
                do {
                    
                    let pulledData = try JSONDecoder().decode(Defaults.self, from: data)
                    
                    let status = pulledData.status
                    
                    if status == 200 {
                        
                        expected.fulfill()
                        
                    }else {
                        XCTFail()
                    }
                    
                }catch{
                    
                    print(error)
                    
                }
                
                
            }else{
                print(error!)
                
            }
            
            }.resume()
        
        waitForExpectations(timeout: 10.0, handler: nil)
    }

//    func testImageDownload() {
//
//        let imageLink = "https://dummyimage.com/300x250/000/fff.png"
//        let expected = expectation(description: "Image from https did load")
//        let viewer = UIImageView(frame: CGRect(x: 0, y: 0, width: 300, height: 250))
//
//        let urlLink = URL(string: imageLink)
//
//
//////
////        viewer.downloadedFrom(link: "http://i.imgur.com/NMiVuDcs.jpg") {
////            if viewer.image != nil {
////
////                expected.fulfill()
////
////            }else{
////                XCTFail()
////            }
////        }
//
//        waitForExpectations(timeout: 20.0, handler: nil)
//
//    }

//    func test_DateImageLoadedFromHTTPSURL() {
//
//        let expected = expectation(description: "Image from https did load")
//
//        let viewer = UIImageView(frame: CGRect(x: 0, y: 0, width: 300, height: 250))
//
//
//        if viewer.image != nil {
//            expected.fulfill()
//        } else {
//            XCTFail()
//        }
//
//        waitForExpectations(timeout: 3.0, handler: nil)
//
//    }
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
