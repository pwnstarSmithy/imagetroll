//
//  ImageSliderViewController.swift
//  ImageTroll
//
//  Created by pwnstarSmithy on 20/09/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit
import SwiftSpinner

class ImageSliderViewController: UIViewController {
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.frame = view.frame
        
        if sentFromViral == true {
            
           downloadImages()
            
        }else if sentFromTopic == true {
            
            downloadTopicImages()
            
        }else{
            
            print("ooOOOps, nothing to see here, move along!")
            
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func downloadTopicImages() {
        
        //start animation using SwiftSpinner
        SwiftSpinner.show("firing up engines, say goodbye to earth")
        
        for image in 0..<defaultSelectedImage.count {
            let imageView = UIImageView()
            
            let imageLinkage = defaultSelectedImage[image]
            
            imageView.contentMode = .scaleAspectFit
            //            let imageLink = image.link
            //            let imageUrl = URL(string: imageLink!)
            //            imageView.downloadedFrom(url: imageUrl!)
            
            let xPosition = self.view.frame.width * CGFloat(image)
            
            imageView.frame = CGRect(x: xPosition, y: 0, width: self.scrollView.frame.width, height: self.scrollView.frame.height)
            
            scrollView.contentSize.width = scrollView.frame.width * CGFloat(image + 1)
            scrollView.addSubview(imageView)
            
            
            if imageLinkage.type == "image/png"{
                
                let imageLink = imageLinkage.link
                let editedUrl = imageLink?.replacingOccurrences(of: ".png", with: "m.png")
                let imageUrl = URL(string: editedUrl!)
                imageView.downloadedFrom(url: imageUrl!)
                
                
            }else if imageLinkage.type == "image/jpeg" {
                
                let imageLink = imageLinkage.link
                let editedUrl = imageLink?.replacingOccurrences(of: ".jpg", with: "m.jpg")
                let imageUrl = URL(string: editedUrl!)
                imageView.downloadedFrom(url: imageUrl!)
                
            }else if imageLinkage.type == "video/mp4"{
                
                let imageLink = imageLinkage.link
                let editedUrl = imageLink?.replacingOccurrences(of: ".mp4", with: "m.mp4")
                let imageUrl = URL(string: editedUrl!)
                imageView.downloadedFrom(url: imageUrl!)
                
            }else {
                let imageLink = imageLinkage.link
                let editedUrl = imageLink?.replacingOccurrences(of: ".gif", with: "m.gif")
                let imageUrl = URL(string: editedUrl!)
                imageView.downloadedFrom(url: imageUrl!)
                
                
            }
            
        }
        SwiftSpinner.hide()
    }
    
    func downloadImages() {
        
        //start animation using SwiftSpinner
        SwiftSpinner.show("firing up engines, say goodbye to earth")
        
        for image in 0..<selectedImages.count {
            let imageView = UIImageView()
            
            let imageLinkage = selectedImages[image]
            
            imageView.contentMode = .scaleAspectFit
//            let imageLink = image.link
//            let imageUrl = URL(string: imageLink!)
//            imageView.downloadedFrom(url: imageUrl!)
            
            let xPosition = self.view.frame.width * CGFloat(image)
            
            imageView.frame = CGRect(x: xPosition, y: 0, width: self.scrollView.frame.width, height: self.scrollView.frame.height)
            
            scrollView.contentSize.width = scrollView.frame.width * CGFloat(image + 1)
            scrollView.addSubview(imageView)
            

                if imageLinkage.type == "image/png"{
                    
                    let imageLink = imageLinkage.link
                    let editedUrl = imageLink?.replacingOccurrences(of: ".png", with: "m.png")
                    let imageUrl = URL(string: editedUrl!)
                    imageView.downloadedFrom(url: imageUrl!)
  
                    
                }else if imageLinkage.type == "image/jpeg" {
                    
                    let imageLink = imageLinkage.link
                    let editedUrl = imageLink?.replacingOccurrences(of: ".jpg", with: "m.jpg")
                    let imageUrl = URL(string: editedUrl!)
                    imageView.downloadedFrom(url: imageUrl!)
                    
                }else if imageLinkage.type == "video/mp4"{
                    
                    let imageLink = imageLinkage.link
                    let editedUrl = imageLink?.replacingOccurrences(of: ".mp4", with: "m.mp4")
                    let imageUrl = URL(string: editedUrl!)
                    imageView.downloadedFrom(url: imageUrl!)
                    
                }else {
                    let imageLink = imageLinkage.link
                    let editedUrl = imageLink?.replacingOccurrences(of: ".gif", with: "m.gif")
                    let imageUrl = URL(string: editedUrl!)
                    imageView.downloadedFrom(url: imageUrl!)
             
                    
                }
 
        }
        
        SwiftSpinner.hide()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
