//
//  DefaultsViewController.swift
//  ImageTroll
//
//  Created by pwnstarSmithy on 18/09/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit
import SwiftSpinner

var imageId : String?

var selectedImages = [ViralImage]()

var sentFromViral : Bool?

class DefaultsViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    
    var section : String?
    var sort : String?
    var page : String?
    var viralBool : Bool?
    
    
    @IBAction func sortImages(_ sender: Any) {
        
        let alertController = UIAlertController(title: "Filter These images", message: nil, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "Popular", style: .default, handler: { action in
            
            self.section = "hot"
            
            self.getViral()
            
        }))
        
        alertController.addAction(UIAlertAction(title: "Newest", style: .default, handler: { action in
           
            self.section = "top"
            
            self.getViral()
            
        }))

        alertController.addAction(UIAlertAction(title: "Show Viral Images", style: .default, handler: { action in
           
            self.viralBool = true
            
            self.getViral()
        }))
        
        alertController.addAction(UIAlertAction(title: "Hide Viral Images", style: .default, handler: { action in
            
            self.viralBool = false
            
            self.getViral()
        }))

        alertController.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { action in
            
        }))
        self.present(alertController, animated: true, completion: nil )
    }
    
    
    var viralArray = [ViralData]()
    var topPost : TopPost?
    var heroImage : HeroImage?
    
    @IBOutlet weak var collectionVIew: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fixCache()
        
        //assign URL params
        section = "hot"
        sort = "viral"
        page = "1"
        viralBool = true
        
        let layout = collectionVIew.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsetsMake(0, 5, 0, 5)
        layout.minimumInteritemSpacing = 5
        layout.itemSize = CGSize(width: (self.collectionVIew.frame.size.width - 20)/2, height: self.collectionVIew.frame.size.height/3)
        
        
        collectionVIew.dataSource = self
        collectionVIew.delegate = self
        
    self.getViral()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func fixCache() {
        let memoryCapacity = 500 * 1024 * 1024
        let diskCapacity = 500 * 1024 * 1024
        let urlCache = URLCache(memoryCapacity: memoryCapacity, diskCapacity: diskCapacity, diskPath: "myDiskPath")
        URLCache.shared = urlCache
    }
    func getViral() {
        
        //start animation using SwiftSpinner
        SwiftSpinner.show("Moving satelites into position")
        
        let url = NSURL(string: "https://api.imgur.com/3/gallery/\(section!)/\(sort!)/\(page!)?showViral=\(viralBool!)")!
        
//        "https://api.imgur.com/3/gallery/\(section)/\(sort)/\(page)?showViral=\(viralBool)"
        
        let tokenString = "Bearer " + accessToken
        
        var request = URLRequest(url: url as URL)
        
        request.setValue(tokenString, forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        //launch the session
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let data = data else {return}
            
            
            if error == nil {
                
                do {
                    
                    let pulledData = try JSONDecoder().decode(Viral.self, from: data)
                    
                    self.viralArray = pulledData.data!
          
                    for processedViral in self.viralArray {
                    
                        
                    }
                    
                    DispatchQueue.main.async(execute: {
                        
                        self.collectionVIew.reloadData()
                    })
                    
                    SwiftSpinner.hide()
                    
                }catch{
                    
                    SwiftSpinner.hide()
                    print(error)
                    
                }
                
                
            }else{
                
                SwiftSpinner.hide()
                print(error!)
                
            }
            
            }.resume()
        
        
    }
    
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viralArray.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DefaultsCell", for: indexPath) as! DefaultsCollectionViewCell

        
        cell.featuredTitle.text = viralArray[indexPath.row].title
        
        let imageArray = viralArray[indexPath.row].images
      
    
        if imageArray != nil {
            
           
            //return the first image in the array
            let firstImage = imageArray?.first
            
            //decrease the size of the images downloaded from Imagur to speed up the app
            
            if firstImage?.type == "image/png"{
                
                let urlString = firstImage?.link
                
                let editedUrl = urlString?.replacingOccurrences(of: ".png", with: "s.png")
                
                let imageUrl = URL(string: editedUrl!)
                
                cell.featuredImage.downloadedFrom(url: imageUrl!)
                
            }else if firstImage?.type == "image/jpeg" {
                
                let urlString = firstImage?.link
                
                let editedUrl = urlString?.replacingOccurrences(of: ".jpg", with: "s.jpg")
                
                let imageUrl = URL(string: editedUrl!)
                
                cell.featuredImage.downloadedFrom(url: imageUrl!)
            
            }else if firstImage?.type == "video/mp4"{
            
                let urlString = firstImage?.link
                
                let editedUrl = urlString?.replacingOccurrences(of: ".mp4", with: "s.mp4")
                
                let imageUrl = URL(string: editedUrl!)
                
                cell.featuredImage.downloadedFrom(url: imageUrl!)
                
            }else {
                let urlString = firstImage?.link
                
                let editedUrl = urlString?.replacingOccurrences(of: ".gif", with: "s.gif")
                
                let imageUrl = URL(string: editedUrl!)
                
                cell.featuredImage.downloadedFrom(url: imageUrl!)
             
                
            }
            
            
        }
        
        
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.borderWidth = 0.5
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        sentFromViral = true
        
        if viralArray[indexPath.row].images != nil {
            
            selectedImages = viralArray[indexPath.row].images!
        }
        
    }
    
//
//    // MARK: - Navigation
//
//    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if let destination = segue.destination as? DefaultSelectedViewController {
//
//            destination.defaultId = defaultId
//
//
//        }
//        // Get the new view controller using segue.destinationViewController.
//        // Pass the selected object to the new view controller.
//    }


}
