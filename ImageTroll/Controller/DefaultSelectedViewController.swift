//
//  DefaultSelectedViewController.swift
//  ImageTroll
//
//  Created by pwnstarSmithy on 18/09/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit
import SwiftSpinner


var defaultSelectedImage = [Images]()

var sentFromTopic: Bool?

class DefaultSelectedViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    var layoutFormat : String?
    
    var imagesArray = [ImageArray]()
    
    @IBAction func changeLayout(_ sender: Any) {
        
        if layoutFormat == "gridLayout"{
            
            
            listLayout()
            
            
            
        }else{
            gridLayout()
        }
        
        
    }
    
    
    @IBOutlet weak var collectionVIew: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        print("This is the id \(defaultId!)")
        collectionVIew.dataSource = self
        collectionVIew.delegate = self
        
        fixCache()
        loadImages()
        
        gridLayout()
        
        // Do any additional setup after loading the view.
    }
    
    func gridLayout(){
        
        layoutFormat = "gridLayout"
        let layout = collectionVIew.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsetsMake(0, 5, 0, 5)
        layout.minimumInteritemSpacing = 5
        layout.itemSize = CGSize(width: (self.collectionVIew.frame.size.width - 20)/2, height: self.collectionVIew.frame.size.height/3)
        
        
    }
    
    func listLayout(){
        layoutFormat = "listLayout"
        
        let layout = collectionVIew.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsetsMake(0, 5, 0, 5)
        layout.minimumInteritemSpacing = 1
        layout.itemSize = CGSize(width: (self.collectionVIew.frame.size.width - 20)/1, height: self.collectionVIew.frame.size.height/3)
        
    }
    
//    func staggeredLayout(){
//
//        //because all images have the same height, i have randomize the height in order to get staggaered layout.
//
//        layoutFormat = "staggeredLayout"
//
//        let layout = collectionVIew.collectionViewLayout as! UICollectionViewFlowLayout
//        layout.sectionInset = UIEdgeInsetsMake(0, 5, 0, 5)
//        layout.minimumInteritemSpacing = 1
//        layout.itemSize = CGSize(width: (self.collectionVIew.frame.size.width - 20)/2, height: self.collectionVIew.frame.size.height)
//
//    }
    func fixCache() {
        let memoryCapacity = 500 * 1024 * 1024
        let diskCapacity = 500 * 1024 * 1024
        let urlCache = URLCache(memoryCapacity: memoryCapacity, diskCapacity: diskCapacity, diskPath: "myDiskPath")
        URLCache.shared = urlCache
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func loadImages(){
        
        //start animation using SwiftSpinner
        SwiftSpinner.show("Distorting space time continuum")
        
        let viral = "viral"
        let topicId = defaultId
        let pageNumber = 1
        
        let url = NSURL(string: "https://api.imgur.com/3/topics/\(topicId!)/\(viral)/\(pageNumber)")!
        
        let tokenString = "Bearer " + accessToken
        
        var request = URLRequest(url: url as URL)
        
        request.setValue(tokenString, forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        //launch the session
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let data = data else {return}
            
            
            if error == nil {
                
                do {
                    
                    let pulledData = try JSONDecoder().decode(Topic.self, from: data)
                    
                    self.imagesArray = pulledData.data!
            
                    for processedImages in self.imagesArray {
                        
                        
                        
                    }
                    
                    DispatchQueue.main.async(execute: {
                        
                        self.collectionVIew.reloadData()
                    })
                    SwiftSpinner.hide()
                }catch{
                    
                    SwiftSpinner.hide()
                    print(error)
                    
                }
                
            }else{
                SwiftSpinner.hide()
                print(error!)
                
            }
            
            }.resume()
        
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imagesArray.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MoreImagesCell", for: indexPath) as! DefaultSelectedCollectionViewCell
        
        
        cell.selectedCaption.text = imagesArray[indexPath.row].title
        
//        if imagesArray[indexPath.row].link != nil {
//
//            let urlString = imagesArray[indexPath.row].link
//
//            let imageUrl = URL(string: urlString!)
//
//            cell.selectedImage.downloadedFrom(url: imageUrl!)
//
//
//        }
        
        if imagesArray[indexPath.row].images != nil {
            
            let imageArray = imagesArray[indexPath.row].images!
            
            //return the first image in the array
            let firstImage = imageArray.first
            
            
            //decrease the size of images downloaded from Imagur to make app faster
            if firstImage?.type == "image/png"{
                let urlString = firstImage?.link
                
                let editedUrl = urlString?.replacingOccurrences(of: ".png", with: "s.png")
           
                let imageUrl = URL(string: editedUrl!)
            
                
                cell.selectedImage.downloadedFrom(url: imageUrl!)
                
            }else if firstImage?.type == "image/jpeg"{
                
                let urlString = firstImage?.link
                
                let editedUrl = urlString?.replacingOccurrences(of: ".jpg", with: "s.jpg")
               
                let imageUrl = URL(string: editedUrl!)
                
                
                cell.selectedImage.downloadedFrom(url: imageUrl!)
            }else{
                
                let urlString = firstImage?.link
                
                let editedUrl = urlString?.replacingOccurrences(of: ".gif", with: "s.gif")
              
                let imageUrl = URL(string: editedUrl!)
                
                
                cell.selectedImage.downloadedFrom(url: imageUrl!)
            }
            

            
        }else{
            
            
            
            let urlString = imagesArray[indexPath.row].link
            
            
            
            let editedUrl = urlString?.replacingOccurrences(of: ".jpg", with: "s.jpg")
        
            let imageUrl = URL(string: editedUrl!)
            
            cell.selectedImage.downloadedFrom(url: imageUrl!)
            
        }
        
                cell.layer.borderColor = UIColor.lightGray.cgColor
                cell.layer.borderWidth = 0.5
        

        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        sentFromTopic = true
        
        if imagesArray[indexPath.row].images != nil {
            
            defaultSelectedImage = imagesArray[indexPath.row].images!
        }
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
