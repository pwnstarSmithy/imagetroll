//
//  ViewController.swift
//  ImageTroll
//
//  Created by pwnstarSmithy on 17/09/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit
import SwiftSpinner

var defaultId : Int?


//Declare variable to cache images.
var postImageCache = NSCache<NSString, UIImage>()

//extension to download images from internet
extension UIImageView {
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFill) {
        contentMode = mode
        //make sure the image placeholder is emptied before checking cache for  image
        image = nil
        //check if image is in cache before loading it from the internet
        if let imageFromCache = postImageCache.object(forKey: url.absoluteString as NSString){
            
            self.image = imageFromCache
            return
        }
        
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                
                let imageToCache = image
                
                postImageCache.setObject(imageToCache, forKey: url.absoluteString as NSString)
                
                self.image = imageToCache
            }
            }.resume()
    }
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFill) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
}



class ViewController: UIViewController {

    var interestsArray = [FirstData]()
    var topPost : TopPost?
    var heroImage : HeroImage?
    @IBOutlet weak var collectionView: UICollectionView!
    
    let cellScaling: CGFloat = 0.6
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fixCache()
        
        let screenSize = UIScreen.main.bounds.size
        let cellWidth = floor(screenSize.width * cellScaling)
        let cellHeight = floor(screenSize.height * cellScaling)
        
        let insetX = (view.bounds.width - cellWidth) / 2.0
        let insetY = (view.bounds.height - cellHeight) / 2.0
        
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: cellWidth, height: cellHeight)
        
        collectionView.contentInset = UIEdgeInsets(top: insetY, left: insetX, bottom: insetY, right: insetX)
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        self.getInterests()
        // Do any additional setup after loading the view, typically from a nib.
    }

    func fixCache() {
        let memoryCapacity = 500 * 1024 * 1024
        let diskCapacity = 500 * 1024 * 1024
        let urlCache = URLCache(memoryCapacity: memoryCapacity, diskCapacity: diskCapacity, diskPath: "myDiskPath")
       URLCache.shared = urlCache
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func getInterests() {
        
        //start animation using SwiftSpinner
        SwiftSpinner.show("Launching escape pods")
        
        let url = NSURL(string: "https://api.imgur.com/3/topics/defaults")!
        
        let tokenString = "Bearer " + accessToken
        
        var request = URLRequest(url: url as URL)
        
        request.setValue(tokenString, forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        //launch the session
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let data = data else {return}
            
            
            if error == nil {
                
                do {
                    
                    let pulledData = try JSONDecoder().decode(Defaults.self, from: data)
                    
                    self.interestsArray = pulledData.data!
                    
                    
                    for processedInterests in self.interestsArray {
                        
                        
                        
                    }
                 
                    
                    DispatchQueue.main.async(execute: {
                        
                        self.collectionView.reloadData()
                    })
                    SwiftSpinner.hide()
                }catch{
                    
                    SwiftSpinner.hide()
                    print(error)
                    
                }
                
                
            }else{
                SwiftSpinner.hide()
                print(error!)
                
            }
            
            }.resume()
        
        
    }
    

}


extension ViewController : UICollectionViewDataSource
{

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return interestsArray.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "InterestCell", for: indexPath) as! HomeCollectionViewCell
        
        cell.interestTitleLable.text = interestsArray[indexPath.row].description
        
        
        let topPostImageArray = interestsArray[indexPath.row].topPost?.images
        
        
        
        if topPostImageArray != nil {
            
            
            //return the first image in the array
            let firstImage = topPostImageArray?.first
            
            //decrease the size of the images downloaded from Imagur to speed up the app
            
            if firstImage?.type == "image/png"{
                
                let urlString = firstImage?.link
                
                let editedUrl = urlString?.replacingOccurrences(of: ".png", with: "m.png")
                
                let imageUrl = URL(string: editedUrl!)
                
                cell.featuredImage.downloadedFrom(url: imageUrl!)
                
            }else if firstImage?.type == "image/jpeg" {
                
                let urlString = firstImage?.link
                
                let editedUrl = urlString?.replacingOccurrences(of: ".jpg", with: "m.jpg")
                
                let imageUrl = URL(string: editedUrl!)
                
                cell.featuredImage.downloadedFrom(url: imageUrl!)
            }else {
                let urlString = firstImage?.link
                
                let editedUrl = urlString?.replacingOccurrences(of: ".gif", with: "m.gif")
                
                let imageUrl = URL(string: editedUrl!)
                
                cell.featuredImage.downloadedFrom(url: imageUrl!)
                
                
            }
            
            
        }else{
            
            if interestsArray[indexPath.row].heroImage?.link != nil {
                
                let urlString = interestsArray[indexPath.row].heroImage?.link
                
                let cardURL = URL(string: urlString!)
                
                cell.featuredImage.downloadedFrom(url: cardURL!)
                
                
            }else if interestsArray[indexPath.row].topPost?.link != nil {
                let urlString = interestsArray[indexPath.row].topPost?.link
                
                let cardURL = URL(string: urlString!)
                
                cell.featuredImage.downloadedFrom(url: cardURL!)
                
                
            }else{
                
                print("no image for default")
                
            }
            
            
        }
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        defaultId = interestsArray[indexPath.row].id
        
    }
    
    
    
}

extension ViewController : UICollectionViewDelegate, UIScrollViewDelegate
{
    
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        let layout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let cellWidthIncludingSpacing = layout.itemSize.width + layout.minimumLineSpacing
        
        var offset =  targetContentOffset.pointee
        let index = (offset.x + scrollView.contentInset.left) / cellWidthIncludingSpacing
        
        let roundedIndex = round(index)
        offset = CGPoint(x: roundedIndex * cellWidthIncludingSpacing - scrollView.contentInset.left , y: -scrollView.contentInset.top)
        targetContentOffset.pointee = offset
        
        
        
    }
    
    
    
}



