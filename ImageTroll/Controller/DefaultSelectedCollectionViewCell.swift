//
//  DefaultSelectedCollectionViewCell.swift
//  ImageTroll
//
//  Created by pwnstarSmithy on 18/09/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class DefaultSelectedCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var selectedImage: UIImageView!
    
    @IBOutlet weak var selectedCaption: UILabel!
}
