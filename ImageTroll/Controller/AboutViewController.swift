//
//  AboutViewController.swift
//  ImageTroll
//
//  Created by pwnstarSmithy on 20/09/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {

    @IBOutlet weak var appName: UILabel!
    
    @IBOutlet weak var versionLabel: UILabel!
    
    @IBOutlet weak var buildLabel: UILabel!
    
    @IBOutlet weak var emailAddress: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Get display name, version and build
        
        if let displayName = Bundle.main.localizedInfoDictionary?["CFBundleDisplayName"] as? String {
            
            appName.text = displayName
//            self.displayName = displayName
        }
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            
            versionLabel.text = "Version \(version)"

        }
        if let build = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            
            buildLabel.text = "Build \(build)"

        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
