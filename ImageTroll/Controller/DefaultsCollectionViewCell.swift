//
//  DefaultsCollectionViewCell.swift
//  ImageTroll
//
//  Created by pwnstarSmithy on 18/09/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class DefaultsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var featuredImage: UIImageView!
    
    @IBOutlet weak var featuredTitle: UILabel!
    
}
