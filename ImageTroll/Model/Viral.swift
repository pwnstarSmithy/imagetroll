//
//  Viral.swift
//  ImageTroll
//
//  Created by pwnstarSmithy on 19/09/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import Foundation
struct Viral : Decodable {

    let data : [ViralData]?
    let status : Int?
    
}

struct ViralData : Decodable {
    
    let id : String?
    let Name : String?
    let title : String?
    let description : String?
    let link : String?
    let images : [ViralImage]?
    
//    let topPost : TopPost?
//    let heroImage : HeroImage?
    
}

struct ViralImage : Decodable {
    let id : String?
    let link : String?
    let type : String?
    
}
