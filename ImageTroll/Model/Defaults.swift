//
//  Defaults.swift
//  ImageTroll
//
//  Created by pwnstarSmithy on 17/09/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import Foundation

struct Defaults : Decodable {
    
    let data : [FirstData]?
    let status : Int?
    let success : Bool?
}

struct FirstData : Decodable {
    
    let id : Int?
    let name : String?
    let description : String?
    let link : String?
    let topPost : TopPost?
    let heroImage : HeroImage?

}

struct TopPost : Decodable {
    let link : String?
    let cover : String?
    let images : [TopImage]?
}

struct HeroImage : Decodable {
    let link : String?
}

struct TopImage : Decodable {
    let id : String?
    let link : String?
    let type : String?
}
